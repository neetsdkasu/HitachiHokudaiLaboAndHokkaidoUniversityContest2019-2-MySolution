// Try Hitachi Hokudai Labo & Hokkaido University Contest 2019-2
// author: Leonardone @ NEETSDKASU

macro_rules! ewriteln {
    ($fmt:expr)  => {{
        use std::io::Write;
        writeln!(&mut std::io::stderr(), $fmt).unwrap();
    }};
    ($fmt:expr, $($arg:tt)*) => {{
        use std::io::Write;
        writeln!(&mut std::io::stderr(), $fmt, $($arg)*).unwrap();
    }};
}

fn main() {
    let start_time = std::time::SystemTime::now();
    let time_limit = std::time::Duration::from_millis(29500);

    let io = StandardIO::new();
    let mut io = io.get_io();

    let problem = Problem::read(&mut io);

    if cfg!(debug_assertions) {
        ewriteln!("vertex_count: {}", problem.vertex_count);
        let far_from1 = problem.fw_table[SHOP_ID][2..]
            .iter()
            .enumerate()
            .max_by(|u, v| u.1.distance.cmp(&v.1.distance))
            .unwrap();
        ewriteln!(
            "far from 1: id({}), distance({})",
            far_from1.0 + 2,
            far_from1.1.distance
        );
        let most_far = problem.fw_table[1..]
            .iter()
            .enumerate()
            .map(|(i, row)| {
                let (j, conn) = row[1..]
                    .iter()
                    .enumerate()
                    .max_by(|u, v| u.1.distance.cmp(&v.1.distance))
                    .unwrap();
                (i, j, conn)
            })
            .max_by(|u, v| u.2.distance.cmp(&v.2.distance))
            .unwrap();
        ewriteln!(
            "most far pair: id({}) id({}) distance({})",
            most_far.0 + 1,
            most_far.1 + 1,
            most_far.2.distance
        );
    }

    let mut solver = SolverC::new(start_time, time_limit, &problem);
    let mut achived_orders = vec![];
    let mut verdict = Verdict::Ok;
    for time in 0..problem.time_max {
        let status = read_status(&mut io);
        let jam_status = read_jam_status(&mut io);
        let cancel_orders = read_cancel_orders(&mut io);
        let new_orders = read_new_orders(&mut io);
        let put_orders = read_put_orders(&mut io);
        let cmd = solver.get_command(
            time,
            (status, jam_status, verdict),
            &new_orders,
            &cancel_orders,
            &put_orders,
            &achived_orders,
        );
        io.println(cmd);
        verdict = read_verdict(&mut io);
        if let Verdict::Ng = verdict {
            break;
        }
        achived_orders = read_achived_orders(&mut io);
    }

    ewriteln!("solution time: {:?}", start_time.elapsed().unwrap());
}

type OrderID = usize;
type VertexID = usize;
type Distance = i32;
const DISATANCE_INFINITY: Distance = std::i32::MAX / 4;
const SHOP_ID: VertexID = 1;
const NONE_VERTEX_ID: VertexID = 0;

struct SolverC<'a> {
    _start_time: std::time::SystemTime,
    _time_limit: std::time::Duration,
    problem: &'a Problem,
    commands: Vec<Connection>,
    command_pos: usize,
    orders: std::collections::BTreeMap<VertexID, i32>,
    order_table: std::collections::HashMap<OrderID, VertexID>,
}

impl<'a> SolverC<'a> {
    fn new(stime: std::time::SystemTime, limit: std::time::Duration, prob: &'a Problem) -> Self {
        SolverC::<'a> {
            _start_time: stime,
            _time_limit: limit,
            problem: prob,
            commands: vec![],
            command_pos: 0,
            orders: std::collections::BTreeMap::new(),
            order_table: std::collections::HashMap::new(),
        }
    }
    fn get_command(
        &mut self,
        time: usize,
        (status, _jam_status, verdict): (Status, Vec<bool>, Verdict),
        new_orders: &[Order],
        cancel_orders: &[OrderID],
        put_orders: &[OrderID],
        achived_orders: &[OrderID],
    ) -> Command {
        self.update_orders(new_orders, cancel_orders, put_orders, achived_orders);
        if self.need_to_stay(status, verdict) {
            return Command::Stay;
        }
        if let Some(id) = self.next_move_to() {
            return Command::Move(id);
        }
        if self.orders.is_empty() {
            return Command::Stay;
        }
        self.make_commands(time);
        self.next_move_to().map_or(Command::Stay, Command::Move)
    }
    fn need_to_stay(&mut self, status: Status, verdict: Verdict) -> bool {
        match verdict {
            Verdict::Ok => {}
            Verdict::Ng => return true,
            Verdict::Jam => {
                if let Some(conn) = self.commands.get_mut(self.command_pos) {
                    conn.distance += 1;
                }
            }
            Verdict::Wait(_) => return true,
        };
        match status {
            Status::NotBroken => {}
            Status::Broken => return true,
            Status::Warning(time) => {
                if self.command_pos >= self.commands.len() {
                    return false;
                }
                let mut dist = 0;
                for i in self.command_pos..self.commands.len() {
                    dist += self.commands[i].distance;
                    if self.commands[i].vertex_id == SHOP_ID {
                        break;
                    }
                }
                if dist <= time as Distance {
                    return false;
                }
                let next_conn = self.commands[self.command_pos].clone();
                let return_dist = self.problem.fw_table[next_conn.vertex_id][SHOP_ID].distance;
                if return_dist + next_conn.distance > time as Distance {
                    return false;
                }
                self.command_pos = 0;
                self.commands.clear();
                self.commands.push(next_conn.clone());
                let path = self.problem.get_path(next_conn.vertex_id, SHOP_ID);
                self.commands.extend_from_slice(&path);
            }
        };
        false
    }
    fn update_orders(
        &mut self,
        new_orders: &[Order],
        cancel_orders: &[OrderID],
        put_orders: &[OrderID],
        achived_orders: &[OrderID],
    ) {
        let orders = &mut self.orders;
        let order_table = &mut self.order_table;
        for order in new_orders {
            order_table.insert(order.id, order.destination);
        }
        for order_id in cancel_orders.iter() {
            let vertex_id = order_table.get(order_id).unwrap();
            if let std::collections::btree_map::Entry::Occupied(mut oe) = orders.entry(*vertex_id) {
                *oe.get_mut() -= 1;
                if *oe.get() == 0 {
                    oe.remove();
                }
            }
        }
        for order_id in achived_orders.iter() {
            let vertex_id = order_table.get(order_id).unwrap();
            if let std::collections::btree_map::Entry::Occupied(mut oe) = orders.entry(*vertex_id) {
                *oe.get_mut() -= 1;
                if *oe.get() == 0 {
                    oe.remove();
                }
            }
        }
        for order_id in put_orders.iter() {
            let vertex_id = order_table.get(order_id).unwrap();
            *orders.entry(*vertex_id).or_insert(0) += 1;
        }
    }
    fn next_move_to(&mut self) -> Option<VertexID> {
        self.commands
            .get_mut(self.command_pos)
            .map(|conn| {
                if conn.distance == 0 {
                    None
                } else {
                    conn.distance -= 1;
                    Some(conn.vertex_id)
                }
            })
            .and_then(|id| {
                id.or_else(|| {
                    self.command_pos += 1;
                    self.next_move_to()
                })
            })
    }
    fn make_commands(&mut self, time: usize) {
        let mut ids = vec![
            SHOP_ID, SHOP_ID, SHOP_ID, SHOP_ID, SHOP_ID, SHOP_ID, SHOP_ID,
        ];
        ids.extend_from_slice(&self.orders.keys().cloned().collect::<Vec<_>>());
        ids.push(SHOP_ID);
        self.optimize_path(&mut ids);
        ids.dedup();
        {
            let mut best_lp = 0;
            let mut best_rp = ids.len() - 1;
            let mut best_perf = 0.0;
            let mut lp = 0;
            let mut rp = 1;
            let mut dist = 0;
            let mut count = 0;
            while rp < ids.len() {
                dist += self.problem.fw_table[ids[rp - 1]][ids[rp]].distance;
                count += *self.orders.get(&ids[rp]).unwrap_or(&0);
                if ids[rp] == SHOP_ID {
                    let len = rp - lp + 1;
                    if len > 2 {
                        let perf = count as f64 / dist as f64;
                        if perf > best_perf {
                            best_lp = lp;
                            best_rp = rp;
                            best_perf = perf;
                        }
                    }
                    dist = 0;
                    count = 0;
                    lp = rp;
                }
                rp += 1;
            }
            ids = ids[best_lp..best_rp + 1].to_vec();
        }
        self.build_commands(&ids, ids.len());
        let total_time = time
            + self
                .commands
                .iter()
                .map(|conn| conn.distance as usize)
                .sum::<usize>();
        if total_time >= self.problem.time_max {
            ewriteln!("time over");
            self.make_nonback_commands();
        }
        if cfg!(debug_assertions) {
            let fw = &self.problem.fw_table;
            let vc = ids.len() - 2;
            let mut dist = 0;
            for i in 1..ids.len() {
                dist += fw[ids[i - 1]][ids[i]].distance;
            }
            assert!(time + dist as usize == total_time);
            let oc: i32 = ids[1..ids.len() - 1]
                .iter()
                .filter(|i| **i > 1)
                .map(|i| *self.orders.get(i).unwrap())
                .sum();
            let shop = self
                .commands
                .iter()
                .filter(|conn| conn.vertex_id == SHOP_ID)
                .count();
            ewriteln!(
                concat!(
                    "time: {}, ",
                    "vertex: {}, ",
                    "order: {}, ",
                    "dist: {}, ",
                    "vert_rate: {:.3}, ",
                    "deli_rate: {:.3}, ",
                    "shop: {}"
                ),
                time,
                vc,
                oc,
                dist,
                vc as f64 / self.problem.vertex_count as f64,
                oc as f64 / dist as f64,
                shop
            );
        }
    }
    fn make_nonback_commands(&mut self) {
        let mut ids = vec![SHOP_ID];
        ids.extend_from_slice(&self.orders.keys().cloned().collect::<Vec<_>>());
        ids.push(NONE_VERTEX_ID);
        for _ in 0..5 {
            self.optimize_path(&mut ids);
        }
        self.build_commands(&ids, ids.len() - 1);
    }
    fn build_commands(&mut self, ids: &[VertexID], end: usize) {
        self.commands.clear();
        self.command_pos = 0;
        for i in 0..end - 1 {
            let path = self.problem.get_path(ids[i], ids[i + 1]);
            self.commands.extend_from_slice(&path);
        }
    }
    fn optimize_path(&self, ids: &mut [VertexID]) {
        let fw = &self.problem.fw_table;
        macro_rules! dist {
            ($i:expr, $j:expr) => (fw[$i][$j].distance);
            ($i:expr, $j:expr; $($a:expr, $b:expr);*) => (dist!($i, $j) + dist!($($a, $b);*));
        }
        for i in 1..ids.len() - 2 {
            let mut best = i;
            for j in i + 1..ids.len() - 1 {
                if fw[ids[i - 1]][ids[j]].distance < fw[ids[i - 1]][ids[best]].distance {
                    best = j;
                }
            }
            if best != i {
                ids.swap(i, best);
            }
        }
        for p1 in 1..ids.len() - 5 {
            for p2 in p1 + 2..ids.len() - 3 {
                for p3 in p2 + 1..ids.len() - 1 {
                    let x0 = ids[p1 - 1];
                    let x1 = ids[p1];
                    let x2 = ids[p2 - 1];
                    let x3 = ids[p2];
                    let x4 = ids[p3];
                    let x5 = ids[p3 + 1];
                    // x5-x0 <-> x1-x2 <-> x3-x4 <-> x5-x0
                    let bef = dist!(x0, x1; x2, x3; x4, x5);
                    let (aft, sel) = *[
                        // x5-x0 <-> x2-x1 <-> x4-x3 <-> x5-x0
                        (dist!(x0, x2; x1, x4; x3, x5), 0),
                        // x5-x0 <-> x3-x4 <-> x1-x2 <-> x5-x0
                        (dist!(x0, x3; x4, x1; x2, x5), 1),
                        // x5-x0 <-> x4-x3 <-> x1-x2 <-> x5-x0
                        (dist!(x0, x4; x3, x1; x2, x5), 2),
                        // x5-x0 <-> x3-x4 <-> x2-x1 <-> x5-x0
                        (dist!(x0, x3; x4, x2; x1, x5), 3),
                    ]
                    .iter()
                    .min()
                    .unwrap();
                    if aft - bef > 0 {
                        continue;
                    }
                    match sel {
                        0 => {
                            ids[p1..p2].reverse();
                            ids[p2..p3 + 1].reverse();
                        }
                        1 => {
                            ids[p1..p2].reverse();
                            ids[p2..p3 + 1].reverse();
                            ids[p1..p3 + 1].reverse();
                        }
                        2 => {
                            ids[p1..p2].reverse();
                            ids[p1..p3 + 1].reverse();
                        }
                        3 => {
                            ids[p2..p3 + 1].reverse();
                            ids[p1..p3 + 1].reverse();
                        }
                        _ => {}
                    }
                }
            }
        }
        for _ in 0..ids.len() * 2 {
            for lp in 1..ids.len() - 2 {
                for rp in lp + 1..ids.len() - 1 {
                    let lid0 = ids[lp - 1];
                    let lid1 = ids[lp];
                    let rid0 = ids[rp];
                    let rid1 = ids[rp + 1];
                    let bef = dist!(lid0, lid1; rid0, rid1);
                    let aft = dist!(lid0, rid0; lid1, rid1);
                    if aft - bef > 0 {
                        continue;
                    }
                    ids[lp..rp + 1].reverse();
                }
            }
        }
    }
}

enum Status {
    NotBroken,
    Warning(usize),
    Broken,
}

enum Verdict {
    Ok,
    Ng,
    Jam,
    Wait(usize),
}

enum Command {
    Move(VertexID),
    Stay,
}

impl std::fmt::Display for Command {
    fn fmt(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
        match *self {
            Command::Move(v) => v.fmt(formatter),
            Command::Stay => "-1".fmt(formatter),
        }
    }
}

#[derive(Clone)]
struct Connection {
    vertex_id: VertexID,
    distance: Distance,
}

impl Connection {
    fn new(i: VertexID, d: Distance) -> Self {
        Connection {
            vertex_id: i,
            distance: d,
        }
    }
}

struct Problem {
    vertex_count: usize,
    _jam_times: Vec<Vec<usize>>,
    _frequency: Vec<usize>,
    time_max: usize,
    fw_table: Vec<Vec<Connection>>,
}

impl Problem {
    fn read(io: &mut IO) -> Self {
        let (vc, ec) = io.get2();
        let mut fw = vec![];
        for i in 0..vc + 1 {
            let mut tmp = vec![];
            for j in 0..vc + 1 {
                tmp.push(Connection::new(j, DISATANCE_INFINITY));
            }
            tmp[i].distance = 0;
            fw.push(tmp);
        }
        fw[SHOP_ID][SHOP_ID].distance = DISATANCE_INFINITY;
        for _ in 0..ec {
            let (u, v, d, _, _): (VertexID, VertexID, Distance, i32, i32) = io.get5();
            fw[u][v] = Connection::new(v, d);
            fw[v][u] = Connection::new(u, d);
        }
        for i in 0..vc + 1 {
            for j in 0..vc + 1 {
                for k in 0..vc + 1 {
                    let dist = fw[j][i].distance + fw[i][k].distance;
                    let mut tmp = &mut fw[j][k];
                    if tmp.distance > dist {
                        tmp.vertex_id = i;
                        tmp.distance = dist;
                    }
                }
            }
        }
        let freq = io.get_vec();
        let mut jt = vec![];
        for _ in 0..4 {
            jt.push(io.get_vec());
        }
        let _light = io.get_line();
        let _warning = io.get_line();
        let t_max = io.get();
        Problem {
            vertex_count: vc,
            _jam_times: jt,
            _frequency: freq,
            time_max: t_max,
            fw_table: fw,
        }
    }
    fn get_path(&self, start_id: VertexID, goal_id: VertexID) -> Vec<Connection> {
        let fw = &self.fw_table;
        let mut path = vec![];
        let mut stack = vec![];
        stack.push((start_id, goal_id));
        while let Some((s, g)) = stack.pop() {
            let conn = &fw[s][g];
            if conn.vertex_id == g {
                path.push(conn.clone());
                continue;
            }
            stack.push((conn.vertex_id, g));
            stack.push((s, conn.vertex_id));
        }
        path
    }
}

#[derive(Clone)]
struct Order {
    id: OrderID,
    destination: VertexID,
}

impl Order {
    fn new(i: OrderID, d: VertexID) -> Self {
        Order {
            id: i,
            destination: d,
        }
    }
}

fn read_status(io: &mut IO) -> Status {
    let tokens = io.get_vec::<String>();
    match &tokens[0][..] {
        "NOT_BROKEN" => Status::NotBroken,
        "WARNING" => Status::Warning(tokens[1].parse().unwrap()),
        _ => Status::Broken,
    }
}

fn read_jam_status(io: &mut IO) -> Vec<bool> {
    io.get_vec().iter().map(|v| 0 != *v).collect()
}

fn read_verdict(io: &mut IO) -> Verdict {
    let tokens = io.get_vec::<String>();
    match &tokens[0][..] {
        "OK" => Verdict::Ok,
        "JAM" => Verdict::Jam,
        "WAIT" => Verdict::Wait(tokens[1].parse().unwrap()),
        _ => Verdict::Ng,
    }
}

fn read_new_orders(io: &mut IO) -> Vec<Order> {
    let mut orders = vec![];
    let n = io.get();
    for _ in 0..n {
        let (i, d) = io.get2();
        orders.push(Order::new(i, d));
    }
    orders
}

fn read_cancel_orders(io: &mut IO) -> Vec<OrderID> {
    let mut orders = vec![];
    let n = io.get();
    for _ in 0..n {
        orders.push(io.get());
    }
    orders
}

fn read_put_orders(io: &mut IO) -> Vec<OrderID> {
    let mut orders = vec![];
    let n = io.get();
    for _ in 0..n {
        orders.push(io.get());
    }
    orders
}

fn read_achived_orders(io: &mut IO) -> Vec<OrderID> {
    let mut orders = vec![];
    let n = io.get();
    for _ in 0..n {
        orders.push(io.get());
    }
    orders
}

struct StandardIO {
    stdin: std::io::Stdin,
    stdout: std::io::Stdout,
}

struct IO<'a> {
    stdin: std::io::StdinLock<'a>,
    stdout: std::io::StdoutLock<'a>,
}

impl StandardIO {
    fn new() -> Self {
        StandardIO {
            stdin: std::io::stdin(),
            stdout: std::io::stdout(),
        }
    }
    fn get_io(&self) -> IO {
        IO {
            stdin: self.stdin.lock(),
            stdout: self.stdout.lock(),
        }
    }
}

impl<'a> IO<'a> {
    fn get_line(&mut self) -> String {
        let mut line = String::new();
        std::io::BufRead::read_line(&mut self.stdin, &mut line).unwrap();
        line
    }
    fn get<A>(&mut self) -> A
    where
        A: std::str::FromStr,
        A::Err: std::fmt::Debug,
    {
        self.get_line().trim().parse().unwrap()
    }
    fn get2<A, B>(&mut self) -> (A, B)
    where
        A: std::str::FromStr,
        B: std::str::FromStr,
        A::Err: std::fmt::Debug,
        B::Err: std::fmt::Debug,
    {
        let line = self.get_line();
        let mut tokens = line.split_whitespace();
        let a: A = tokens.next().unwrap().parse().unwrap();
        let b: B = tokens.next().unwrap().parse().unwrap();
        (a, b)
    }
    fn get5<A, B, C, D, E>(&mut self) -> (A, B, C, D, E)
    where
        A: std::str::FromStr,
        B: std::str::FromStr,
        C: std::str::FromStr,
        D: std::str::FromStr,
        E: std::str::FromStr,
        A::Err: std::fmt::Debug,
        B::Err: std::fmt::Debug,
        C::Err: std::fmt::Debug,
        D::Err: std::fmt::Debug,
        E::Err: std::fmt::Debug,
    {
        let line = self.get_line();
        let mut tokens = line.split_whitespace();
        let value_a: A = tokens.next().unwrap().parse().unwrap();
        let value_b: B = tokens.next().unwrap().parse().unwrap();
        let value_c: C = tokens.next().unwrap().parse().unwrap();
        let value_d: D = tokens.next().unwrap().parse().unwrap();
        let value_e: E = tokens.next().unwrap().parse().unwrap();
        (value_a, value_b, value_c, value_d, value_e)
    }
    fn get_vec<A>(&mut self) -> Vec<A>
    where
        A: std::str::FromStr,
        A::Err: std::fmt::Debug,
    {
        self.get_line()
            .split_whitespace()
            .map(|v| v.parse().unwrap())
            .collect()
    }
    fn println<A: std::fmt::Display>(&mut self, v: A) {
        use std::io::Write;
        writeln!(&mut self.stdout, "{}", v).unwrap();
        self.stdout.flush().unwrap();
    }
}
